import boto3
import datetime

# --------------------------------------
# AWS Lambda Handler
# --------------------------------------
def lambda_handler(event, context):
    # Define Variable
    delted_snaps = False

    start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M.%S")
    # If running locally then setup boto3 session
    try:
        if env == 'local':
            print('BEGIN - LAMBDA - ebs_delete_snap')
            print('BEGIN - START TIME - {0}'.format(start_time))
            print('Running locally')
            boto3.setup_default_session(profile_name='my_aws_profile')
    except NameError:
        print('BEGIN - LAMBDA - ebs_delete_snap')
        print('BEGIN - START TIME - {0}'.format(start_time))
        print('Running AWS')

    # Create the resource
    ec2_resource = boto3.resource('ec2', region_name='us-east-1')
    # Get the client from the resource
    ec = ec2_resource.meta.client

     # Create describe_instances filter
    Filters=[
        {'Name': 'tag:Backup', 'Values': ['True']},
        {'Name': 'tag:product', 'Values': ['App Name']},
        #{'Name': 'tag:environment', 'Values': ['Stage']},
        {'Name': 'instance-state-name', 'Values': ['running']},
    ]

    # The reservation is the response from the boto3.client.describe_instance call.
    # There will be a reservation for each instance that matches the search filter.
    reservations = ec.describe_instances(
        # Note: Add a row for each tags that you want to filter on.
        Filters=Filters
    ).get(
        # Grab the dict 'Reservations' dict key
        'Reservations', []
    )

    # This section will create an list of instance dicts.
    # The [] in the sum command stores the last value of the list index number
    instances = sum(
        [
            # Loop for each 'Instance found in the reservatations
            # 'i' will be taken and the sum will of the number of instances found
            # will be assigned to 'instances' variable.
            [i for i in r['Instances']]
            # Loop for each reservation
            for r in reservations
            ], [])

    # Building list of Owner ID's.  These will be used to pull list of snapshots
    owner_ids = []
    for r in reservations:
        #print('Owner ID ='), r['OwnerId']
        owner_ids.append(r['OwnerId'])

    # Prints the number of instances by getting the length of the list.
    # EG. 2 instances would have index 0 and 1 and a lenght of 2; 2 instances
    print "Number of the Instances : {0}".format(len(instances))

    # Now loop for each instance found and create a snapshot
    for instance in instances:

        # Get today's date - format YYYY-MM-DD
        delete_on = datetime.date.today().strftime('%Y-%m-%d')

        # Filter on snapshots that have Key: DeleteOn and Value: 'YYYY-MM-DD' (today's date)
        filters = [
            {'Name': 'tag-key', 'Values': ['DeleteOn']},
            {'Name': 'tag-value', 'Values': [delete_on]},
        ]

        # Build the list of snapshots that will be deleted
        snapshots = ec.describe_snapshots(OwnerIds=owner_ids, Filters=filters)

        for snap in snapshots['Snapshots']:
            delted_snaps = True
            print "DELETING instance ({0}) - snapshot {1}".format(instance['InstanceId'],snap['SnapshotId'])
            ec.delete_snapshot(SnapshotId=snap['SnapshotId'])

        if delted_snaps is False:
           print("NO snapshots deleted")

     # Log the end of the Lambda function
    end_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M.%S")
    print('END - END TIME - {0}'.format(end_time))
    print('END - LAMBDA - ebs_delete_snap')

# --------------------------------------
# Main
# --------------------------------------
# Allow you to run on local sandbox for testing
if __name__ == "__main__":
    #print("running __mina__")
    # Connection settings.  Win NOT be used with lambda function
    global env
    env = 'local'

    lambda_handler("test", "test")
else:
    print("Not running __mina__")


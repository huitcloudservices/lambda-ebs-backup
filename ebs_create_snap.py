import boto3
import collections
import datetime

#session = boto3.session.Session(profile_name='admints', region_name='us-east-1')
#boto3.setup_default_session(profile_name='admints')

# --------------------------------------
# AWS Lambda Handler
# --------------------------------------
def lambda_handler(event, context):
    # Default variables
    retention_days_default = 35
    huit_assetid_default = 0
    product_default = "unknown"
    environment_default = "unknown"

    start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M.%S")
    try:
        if env == 'local':
            print('BEGIN - LAMBDA - ebs_create_snap')
            print('BEGIN - START TIME - {0}'.format(start_time))
            print('Running locally')
            boto3.setup_default_session(profile_name='aws_profile')
    except NameError:
        print('BEGIN - LAMBDA - ebs_create_snap')
        print('BEGIN - START TIME - {0}'.format(start_time))
        print('Running AWS')

    # Create the resource
    ec2_resource = boto3.resource('ec2', region_name='us-east-1')
    # Get the client from the resource
    ec = ec2_resource.meta.client

    # Create describe_instances filter
    Filters=[
        {'Name': 'tag:Backup', 'Values': ['True']},
        {'Name': 'tag:product', 'Values': ['App Name']},
        #{'Name': 'tag:environment', 'Values': ['Stage']},
        {'Name': 'instance-state-name', 'Values': ['running']},
    ]

    # The reservation is the response from the boto3.client.describe_instance call.
    # There will be a reservation for each instance that matches the search filter.
    reservations = ec.describe_instances(
        # Note: Add a row for each tags that you want to filter on.
        Filters=Filters
    ).get(
        # Grab the dict 'Reservations' dict key
        'Reservations', []
    )

    # This section will create an list of instance dicts.
    # The [] in the sum command stores the last value of the list index number
    instances = sum(
        [
            # Loop for each 'Instance found in the reservatations
            # 'i' will be taken and the sum will of the number of instances found
            # will be assigned to 'instances' variable.
            [i for i in r['Instances']]
            # Loop for each reservation
            for r in reservations
            ], [])

    # Prints the number of instances by getting the length of the list.
    # EG. 2 instances would have index 0 and 1 and a lenght of 2; 2 instances
    print "Number of the Instances : {0}".format(len(instances))

    # Create a dict to store the tags for an instance.
    # The collenctions.defaultdict is used to simplify
    # the dict management and will create a dict Key if
    # it does NOT exist.
    to_tag = collections.defaultdict(list)

    # Now loop for each instance found and create a snapshot
    for instance in instances:
        try:
            # Grab the snapshot retention specified for this instance
            retention_days = [
                int(t.get('Value')) for t in instance['Tags']
                if t['Key'] == 'Retention'][0]
        except IndexError:
            # Assign a default retention period of  if none is specified.
            retention_days = retention_days_default

        try:
            # Grab the huit_assetid specified for this instance
            huit_assetid = [
                int(t.get('Value')) for t in instance['Tags']
                if t['Key'] == 'huit_assetid'][0]
        except IndexError:
            # Assign a default retention period of  if none is specified.
            huit_assetid = huit_assetid_default

        try:
            # Grab the huit_assetid specified for this instance
            product = [
                t.get('Value') for t in instance['Tags']
                if t['Key'] == 'product'][0]
        except IndexError:
            # Assign a default product value of  if none is specified.
            product = product_default

        try:
            # Grab the environment specified for this instance
            environment = [
                t.get('Value') for t in instance['Tags']
                if t['Key'] == 'environment'][0]
        except IndexError:
            # Assign a default product value of  if none is specified.
            environment = environment_default

        # Loop for each volume (dev = device) for this instance
        for dev in instance['BlockDeviceMappings']:
            if dev.get('Ebs', None) is None:
                # if it's NOT and Ebs volume then give control
                # back to the for loop. EG. Skip the remaining for loop code.
                continue
            vol_id = dev['Ebs']['VolumeId']

            # Loop through the tags to get the Instance Name then create the snapshot
            for name in instance['Tags']:
                # To store the instance tag value
                Instancename = name['Value']
                # To store the instance key value
                key = name['Key']
                # Below the code is create Snapshot name as instance Name
                if key == 'Name':
                    print "Found EBS volume {0} on instance {1}".format(
                        vol_id, instance['InstanceId'])
                    print("Creating Snapshot..."),
                    snap = ec.create_snapshot(
                        VolumeId=vol_id,
                        Description=Instancename,
                    )
                    print "Done: snap {0}".format(snap)

            to_tag['snapshot_id'].append(snap['SnapshotId'])
            #snapshot_id = 'snapshot_id'

            print "Retaining snapshot %s of volume %s from instance %s for %d days" % (
                snap['SnapshotId'],
                vol_id,
                instance['InstanceId'],
                retention_days,

            )
            # this loop will will create tags for the snapshot and apply the tags.
            for snapshot_id in to_tag.keys():
                # Create the date that the snapshot will be deleted
                delete_date = datetime.date.today() + datetime.timedelta(days=retention_days)
                snap = snap['Description'] + str('_')
                # Get current date
                snapshot = snap + str(datetime.date.today())
                # Format the delete date
                delete_fmt = delete_date.strftime('%Y-%m-%d')
                print "Will delete {0} snapshots on {1}".format(len(to_tag[snapshot_id]), delete_fmt)


                snap_found = False
                loop_count = 0
                while snap_found == False and loop_count != 5:
                    # Try to tag the snapshot.  if it's not visible then sleep 1 second.
                    # Cap max tries to 5
                    try:
                        # Increment loop counter by 1
                        loop_count += 1
                        # below code will add the tags to the snapshot.
                        ec.create_tags(
                            Resources=to_tag[snapshot_id],
                            Tags=[
                                {'Key': 'DeleteOn', 'Value': delete_fmt},
                                {'Key': 'Name', 'Value': snapshot},
                                {'Key': 'environment', 'Value': environment},
                                {'Key': 'huit_assetid', 'Value': "{}".format(huit_assetid)},
                                {'Key': 'product', 'Value': product},
                            ]
                        )
                        snap_found = True
                        print('Snapshot Found: {0}'.format(to_tag[snapshot_id]))
                    except:
                        # The snaphost is not visable yet so sleep 1 second.
                        print('Snapshot NOT Found - (1): {0} - Retrying'.format(to_tag[snapshot_id],loop_count))
                        time.sleep(1)
                    else:
                        # Print status
                        print('Successfully tagged snapshot: {0}'.format(to_tag[snapshot_id]))

                # Record status that snapshot tag was not successful
                if snap_found != True:
                    print('Unable to tag snapshot: {0}'.format(to_tag[snapshot_id]))




        # Clear after each loop
        to_tag.clear()

    # Log the end of the Lambda function
    end_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M.%S")
    print('END - END TIME - {0}'.format(end_time))
    print('END - LAMBDA - ebs_create_snap')
# Used to dbug


# --------------------------------------
# Main
# --------------------------------------
# Allow you to run on local sandbox for testing
if __name__ == "__main__":
    #print("running __mina__")
    # Connection settings.  Win NOT be used with lambda function
    global env
    env = 'local'

    lambda_handler("test", "test")
else:
    print("Not running __mina__")
